import 'dart:io';

void main(List<String> arguments) {
  int price = 45;
  var menu;
  print('Welcome');
  print('Please Select Drink Type');
  print('1. Coffee \n2. Tea \n3. Milk \n4. Soda');

  var category = stdin.readLineSync();
  if (category == '1') {
    category = 'Coffee';
    print('Please select menu');
    print('1. Americano \n2. Espresso \n3. Latte \n4. Mocha');
    menu = stdin.readLineSync();
    if(menu == '1') {
      menu = 'Americano';
    } else if(menu == '2') {
      menu = 'Espresso';
    } else if(menu == '3') {
      menu = 'Latte';
    } else if(menu == '4') {
      menu = 'Mocha';
    }
  }

  if (category == '2') {
    category = 'Tea';
    print('Please select menu');
    print('1. Thai \n2. Matcha \n3. Black \n4. Lemon');
    menu = stdin.readLineSync();
    if(menu == '1') {
      menu = 'Thai';
    } else if(menu == '2') {
      menu = 'Matcha';
    } else if(menu == '3') {
      menu = 'Black';
    } else if(menu == '4') {
      menu = 'Lemon';
    }
  }

  if (category == '3') {
    category = 'Milk';
    print('Please select menu');
    print('1. Fresh Milk \n2. Milo \n3. Cocoa \n4. Ovaltine');
    menu = stdin.readLineSync();
    if(menu == '1') {
      menu = 'Fresh Milk';
    } else if(menu == '2') {
      menu = 'Milo';
    } else if(menu == '3') {
      menu = 'Cocoa';
    } else if(menu == '4') {
      menu = 'Ovaltine';
    }
  }

  if (category == '4') {
    category = 'Soda';
    print('Please select menu');
    print('1. Kiwi \n2. Passion Fruit \n3. Apple \n4. Yuzu');
    menu = stdin.readLineSync();
    if(menu == '1') {
      menu = 'Kiwi';
    } else if(menu == '2') {
      menu = 'Passion Fruit';
    } else if(menu == '3') {
      menu = 'Apple';
    } else if(menu == '4') {
      menu = 'Yuzu';
    }
  }

  print('Please select type');
  print('1. Hot \n2. Iced \n3. Frappe');
  var type = stdin.readLineSync();
  if(type == '1'){
    type = 'Hot';
  }

  if(type == '2'){
    price += 5;
    type = 'Iced';
  }

  if(type == '3'){
    price += 10;
    type = 'Frappe';
  }

  print('Please select sweetness level');
  print('1. No Sugar \n2. Less \n3. Normal \n4. Sweet \n5. Very Sweet');
  var level = stdin.readLineSync();
  if(level == '1') {
    level = 'No Sugar';
  }
  if(level == '2') {
    level = 'Less';
  }
  if(level == '3') {
    level = 'Normal';
  }
  if(level == '4') {
    level = 'Sweet';
  }
  if(level == '5') {
    level = 'Very Sweet';
  }

  print('Please select Straw and Lid');
  print('1. Straw \n2. Lid \n3. Straw and Lid');
  var strawandlid = stdin.readLineSync();
  if(strawandlid == '1') {
    strawandlid = 'Straw';
  }
  if(strawandlid == '2') {
    strawandlid = 'Lid';
  }
  if(strawandlid == '3') {
    strawandlid = 'Straw and Lid';
  }

  print('$category $type $menu $level $strawandlid');

  print('Your total price is $price baht');
  print('Please input your money');
  int money = int.parse(stdin.readLineSync()!);
  while(money < price) {
    print('Please input your money');
    int  money = int.parse(stdin.readLineSync()!);
    price -= money;
    int remain = price;
    if(money == price) {
      print('Please wait for your ordered');
      print('Thank you');
      break;
    }
  }
  while(money >= price) {
    price -= money;
    int remain = price;
    if(remain < 0) {
      int exchange = remain - remain - remain;
      print('Exchange : $exchange');
      print('Please wait for your ordered');
      print('Thank you');
      break;
    }
    if(remain == 0) {
      print('Please wait for your ordered');
      print('Thank you');
      break;
    }
  }
}
